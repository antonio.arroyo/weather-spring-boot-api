FROM gradle:7.5.1-jdk11 as builder
EXPOSE 8080
WORKDIR /usr/app

COPY . .
RUN rm build.gradle
RUN mv build-container.gradle build.gradle
RUN gradle bootJar


FROM gradle:7.5.1-jdk11

ARG JAR_FILE=weather-0.0.1-SNAPSHOT.jar

COPY --from=builder /usr/app/build/libs/${JAR_FILE} /app/service.jar

ENTRYPOINT java -jar /app/service.jar

