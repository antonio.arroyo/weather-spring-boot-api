# How to run the project

## With Docker
The easiest way to run the project is using docker,
but for this, you need to have docker installed.

https://www.docker.com/get-started/

> ! Important: You do not need to do something with this
but, when running with docker, `build-container.gradle` is
the gradle file used.

1. Download the project on your machine.
2. Open a terminal in the root of the project.
3. Run `docker-compose up --build`

This will build and run the project for you, and also
it will run mongo docker image. So you do not
need to worry about installing anything. With
`docker-compose up --build` the api and the
mongo service will be started isolated from your
machine.


The project will start on `:8080`.


## Without Docker

If you do not want to use docker, that is possible
too. 

> ! Important: Ignore `build-container.gradle`, that file
is used by docker whenever running the project with
it. Just ignore it. `build.gradle` is the one used if running the project without docker



1. Download your project in your machine.
2. Open a terminal in the root of the project.
3. run `./gradlew bootRun`
4. or if you want a jar, run `./gradlew bootJar`

You do not need to worry about mongo. When running
the project without docker (lets call it locally), an
embedded version of mongo will be used. So you
do not need to install anything.

> I would encourage people to use the docker way
> because as it is isolated it won't dependent
> on anything related to the host.


# How to test the project

## Postman

Find on the root folder of the project the
following file
`Weather Api.postman_collection.json`

You can import that file on postman. So you will
have the collection of request that can be done.

## On the browser

Because all request are using the HTTP Verb GET,
you can make the request through the browser.

Endpoints:
- `localhost:8080/conditions/code/178087`
- `localhost:8080/conditions/city/pyongyang`
- `localhost:8080/request`




*Happy hacking!*





