package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Metric
import com.api.weather.domain.Temperature
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class TemperatureTest  extends  Specification{

    def "Class exist"(){
        expect:
        Temperature != null
    }

    def "Class can be instantiated with arguments and getters are accessible"(){
        Metric metric = new Metric(20, "C", 1)
        Metric imperial = new Metric(70, "F", 1)
        Temperature temperature = new Temperature(metric, imperial)

        expect: "Is instantiated and with getters working"
        temperature != null
        temperature.getImperial().getValue() == 70
        temperature.getMetric().getValue() == 20
    }

}
