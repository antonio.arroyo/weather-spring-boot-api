package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Location
import com.api.weather.domain.Region
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class LocationTest extends  Specification {

    def "Class exist"(){
        expect:
        Location != null
    }

    def "Location is instantiated and getters work"(){
        Region country = new Region("2", "United States")
        Region state = new Region("3", "Pennsylvania")

        Location location = new Location("1234", country , state, "Pittsburgh")

        expect: "location object is not null and getters work"
        location != null
        location.getEnglishName() == "Pittsburgh"
        location.getCountry().getEnglishName() == "United States"
        location.getAdministrativeArea().getEnglishName() == "Pennsylvania"
    }

}
