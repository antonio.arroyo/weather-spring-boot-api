package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.ConditionsCityResponseDTO
import com.api.weather.domain.ConditionsResponseDTO
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class ConditionsCityResponseDTOTest extends Specification {

    ConditionsResponseDTO dto = ConditionsResponseDTO.builder()
            .observationTime("12:12")
            .precipitationType("rain")
            .temperatureCelsius(20.0)
            .temperatureFahrenheit(70.0)
            .weatherDescription("Rainy day with clouds")
            .build()

    ConditionsCityResponseDTO composeDTO = ConditionsCityResponseDTO.builder()
            .conditions(dto)
            .cityName("Pittsburgh")
            .countryName("United States")
            .stateName("Pennsylvania")
            .build()

    def "ConditionsCityResponseDTOT can be build"(){
        expect: "composeDTO is build"
        composeDTO != null
    }

    def "ConditionsCityResponseDTOT city country and state setters work"(){
        expect: "Information is accessible"
        composeDTO.getCityName() == "Pittsburgh"
        composeDTO.getCountryName() == "United States"
        composeDTO.getStateName() == "Pennsylvania"
    }

}
