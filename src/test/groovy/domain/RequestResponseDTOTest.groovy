package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Metric
import com.api.weather.domain.RequestResponseDTO
import com.api.weather.models.Request
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class RequestResponseDTOTest extends Specification {

    def "Class exist, a request list can be set and get"(){
        Metric metric = new Metric(20, "C", 1)

        Request r = new Request()
        r.setId("1")
        r.setUrl("/test/1")
        r.setResponse(metric)

        List<Request> requestList = Arrays.asList(r);

        RequestResponseDTO requestResponseDTO = RequestResponseDTO
                .builder()
                .requestList(requestList)
                .build()

        expect: "object was instantiated with the proper data"
        requestResponseDTO != null
        requestResponseDTO.getRequestList().get(0).getUrl() == "/test/1"

    }
}
