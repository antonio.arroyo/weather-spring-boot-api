package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.ConditionsResponseDTO
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class ConditionsResponseDTOTest extends Specification {

    ConditionsResponseDTO dto = ConditionsResponseDTO.builder()
            .observationTime("12:12")
            .precipitationType("rain")
            .temperatureCelsius(20.0)
            .temperatureFahrenheit(70.0)
            .weatherDescription("Rainy day with clouds")
            .build()

    def "ConditionsResponseDTO exist"(){
        expect:"ConditionsResponseDTO class exist"
        ConditionsResponseDTO != null
    }

    def "ConditionsResponseDTO can be build"(){
        expect: "dto is build"
        dto != null
    }
    def "ConditionsResponseDTO getters are accessible"(){
        expect: "Getters returns the correct value"
        dto.getTemperatureCelsius() == 20.0
        dto.getTemperatureFahrenheit() == 70.0
        dto.getObservationTime() == "12:12"
        dto.getPrecipitationType() == "rain"
        dto.getWeatherDescription() == "Rainy day with clouds"
    }


}
