package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Metric
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class MetricTest extends Specification{

    def "Class exist"(){
        expect:
        Metric != null
    }

    def "Class can be instantiated with args"(){
        Metric metric = new Metric(20, "C", 1)
        expect: "Metric is initiated and getters work"
        metric != null
        metric.getValue() == 20
        metric.getUnit() == "C"
        metric.getUnitType() == 1
    }
}
