package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Region
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class RegionTest extends Specification{

    def "Region can be initiated without args"(){
        Region region = new Region()
        expect: "Object is instantiated"
        region != null
    }

    def "Region can be initiated with arguments and getters work"(){
        Region city = new Region("1", "Pittsburgh")
        expect: "Object is created and info is accessible"
        city != null
        city.getEnglishName() == "Pittsburgh"
        city.getId() == "1"
    }

    def "Region setters work"(){
        Region city = new Region("1", "Pittsburgh")
        city.setEnglishName("Portland")
        expect: "Returns correct english name"
        city.getEnglishName() == "Portland"
    }

}
