package domain

import com.api.weather.WeatherApplication
import com.api.weather.domain.Conditions
import com.api.weather.domain.Metric
import com.api.weather.domain.Temperature
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class ConditionsTest extends Specification {


    def "Conditions exist and can be instantiated with no arguments"(){
        Conditions conditionsNoArgs = new Conditions()
        expect:
        conditionsNoArgs != null
    }

    def "Conditions instantiated with arguments"(){
        Metric metric = new Metric(20, "C", 1)
        Metric imperial = new Metric(70, "F", 1)
        Temperature temperature = new Temperature(metric, imperial)
        Conditions conditionsArgs = new Conditions("12:12",
                "12:12", "Rainy", 1, false, "", false, temperature, "", "")
        expect: "Object constructed and with getters working"
        conditionsArgs != null
        conditionsArgs.getLocalObservationDateTime() == "12:12"
        conditionsArgs.getTemperature().getMetric().getValue() == 20
        conditionsArgs.getWeatherIcon() == 1
        conditionsArgs.getEpochTime() == "12:12"
        conditionsArgs.getWeatherText() == "Rainy"
    }


}
