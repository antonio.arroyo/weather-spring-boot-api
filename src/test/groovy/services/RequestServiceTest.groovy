package services

import com.api.weather.WeatherApplication
import com.api.weather.domain.Metric
import com.api.weather.models.Request
import com.api.weather.services.RequestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class RequestServiceTest extends Specification {

    @Autowired
    private RequestService requestService

    def "Request Service is created"(){
        expect:
        requestService != null
    }

    def "Request Service Save Method is Executed"(){
        Metric metric = new Metric(20, "C", 1)
        Request request = new Request()
        request.setUrl("/test/1")
        request.setResponse(metric)
        Request response = requestService.save(request)
        expect: "Save method to be executed"
        response != null
    }
}
