package services

import com.api.weather.WeatherApplication
import com.api.weather.domain.ConditionsCityResponseDTO
import com.api.weather.domain.ConditionsResponseDTO
import com.api.weather.services.AccuweatherService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class AccuweatherServiceTest  extends  Specification{

    @Autowired
    private AccuweatherService accuweatherService

    def "getConditionWithQuery returns the conditions of a city"(){
        ConditionsCityResponseDTO response = accuweatherService.getConditionWithQuery("pittsburgh pa")

        expect: "Response is successful"
        response != null
        response.getCityName() == "Pittsburgh"
        response.getCountryName() == "United States"
    }


    def "getConditions returns the conditions based on a location code"(){
        ConditionsResponseDTO response = accuweatherService.getConditions("178087")
        expect: "Response is successful"
        response != null
        response.getObservationTime() != null
        response.getTemperatureCelsius() != null
        response.getTemperatureFahrenheit() != null
    }
}
