package controllers

import com.api.weather.WeatherApplication
import com.api.weather.controllers.AccuweatherController
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class AccuweatherControllerTest extends Specification{

    def "AccuweatherController exist"(){
        expect:
        AccuweatherController != null
    }
}
