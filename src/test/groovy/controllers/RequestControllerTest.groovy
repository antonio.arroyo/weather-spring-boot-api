package controllers

import com.api.weather.WeatherApplication
import com.api.weather.controllers.RequestController
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class RequestControllerTest extends Specification{

    def "Request controller class exist"(){
        expect:
        RequestController != null
    }
}
