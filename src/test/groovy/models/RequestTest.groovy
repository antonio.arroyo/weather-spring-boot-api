package models

import com.api.weather.WeatherApplication
import com.api.weather.domain.Metric
import com.api.weather.models.Request
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = WeatherApplication.class)
class RequestTest extends Specification{

    def "Class can be instantiated, getters and setters work"(){
        Metric metric = new Metric(20, "C", 1)

        Request request = new Request()
        request.setId("1")
        request.setUrl("/test/1")
        request.setResponse(metric)

        expect: "getters retrieve proper info"
        request.getId() == "1"
        request.getUrl() == "/test/1"
        request.getResponse() != null
    }
}
