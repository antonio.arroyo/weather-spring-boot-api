package com.api.weather.controllers;

import com.api.weather.domain.RequestResponseDTO;
import com.api.weather.models.Request;
import com.api.weather.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RequestController {
    @Autowired
    private RequestService requestService;


    @GetMapping("/request")
    ResponseEntity<?> getRequests() throws Exception {
        List<Request> requests = requestService.list();

        RequestResponseDTO responseDTO = RequestResponseDTO.builder()
                .requestList(requests).build();

        EntityModel<RequestResponseDTO> model = EntityModel.of(responseDTO);

        WebMvcLinkBuilder linkToConditionsWithCode = WebMvcLinkBuilder
                .linkTo(WebMvcLinkBuilder
                        .methodOn(AccuweatherController.class)
                        .getConditionsWithCode("178087"));

        WebMvcLinkBuilder linkToConditionsWithQuery = WebMvcLinkBuilder
                .linkTo(WebMvcLinkBuilder
                        .methodOn(AccuweatherController.class)
                        .getConditionsWithQuery("Washington DC"));

        model.add(linkToConditionsWithCode.withRel("conditions-with-code"));
        model.add(linkToConditionsWithQuery.withRel("conditions-with-query"));

        return new ResponseEntity<>(model, HttpStatus.OK);
    }

}
