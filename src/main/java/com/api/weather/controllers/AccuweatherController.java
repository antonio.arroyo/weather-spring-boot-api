package com.api.weather.controllers;

import com.api.weather.domain.ConditionsCityResponseDTO;
import com.api.weather.domain.ConditionsResponseDTO;
import com.api.weather.exceptions.ApiError;
import com.api.weather.exceptions.ApiErrorException;
import com.api.weather.services.AccuweatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class AccuweatherController {

    @Autowired
    private AccuweatherService accuweatherService;

    @GetMapping("/conditions/code/{locationCode}")
    ResponseEntity<?> getConditionsWithCode(@PathVariable String locationCode) throws Exception {
        String path = String.format("/conditions/code/%s", locationCode);
        try{
            ConditionsResponseDTO response  = accuweatherService.getConditions(locationCode);
            EntityModel<ConditionsResponseDTO> model = EntityModel.of(response);
            WebMvcLinkBuilder link = WebMvcLinkBuilder
                    .linkTo(WebMvcLinkBuilder.methodOn(this.getClass())
                            .getConditionsWithQuery("pyongyang"));

            model.add(link.withRel("with-query"));
            return new ResponseEntity<>(model, HttpStatus.OK);


        }catch (ApiErrorException e){
            return new ResponseEntity<>(
                    ApiError.builder()
                            .timestamp(new Date())
                            .error(e.getMessage())
                            .status(e.getHttpStatus().value())
                            .path(path)
                            .build(),
                    e.getHttpStatus());
        }
    }

    @GetMapping("/conditions/city/{query}")
    ResponseEntity<?> getConditionsWithQuery(@PathVariable String query) throws Exception {
        String path = String.format("/conditions/city/%s", query);
        try{

            ConditionsCityResponseDTO response = accuweatherService.getConditionWithQuery(query);
            EntityModel<ConditionsCityResponseDTO> model  = EntityModel.of(response);

            WebMvcLinkBuilder link = WebMvcLinkBuilder
                    .linkTo(WebMvcLinkBuilder.methodOn(RequestController.class).getRequests());

            model.add(link.withRel("request-history"));

            return new ResponseEntity<>(model, HttpStatus.OK);
        }catch (ApiErrorException e){

            return new ResponseEntity<>(
                    ApiError.builder()
                            .timestamp(new Date())
                            .error(e.getMessage())
                            .status(e.getHttpStatus().value())
                            .path(path)
                            .build(),
                    e.getHttpStatus());
        }
    }
}
