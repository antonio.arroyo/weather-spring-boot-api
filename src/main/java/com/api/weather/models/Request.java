package com.api.weather.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@NoArgsConstructor
@Document(collection = "request")
public class Request {

    @Id
    private String id;

    @Field("url")
    private String url;

    @Field("response")
    private Object response;

}
