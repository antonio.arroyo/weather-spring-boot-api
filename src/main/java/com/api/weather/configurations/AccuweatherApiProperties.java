package com.api.weather.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix="accuweather.api")
@Configuration("accuweatherApiProperties")
public class AccuweatherApiProperties {

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private String key;

    public AccuweatherApiProperties() {
    }







}
