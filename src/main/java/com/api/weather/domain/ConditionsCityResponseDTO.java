package com.api.weather.domain;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConditionsCityResponseDTO {

    private ConditionsResponseDTO conditions;
    private String cityName;
    private String stateName;
    private String countryName;
}
