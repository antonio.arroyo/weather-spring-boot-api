package com.api.weather.domain;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ConditionsResponseDTO {
    private String observationTime;
    private String weatherDescription;
    private Double temperatureCelsius;
    private Double temperatureFahrenheit;
    private String precipitationType;
}
