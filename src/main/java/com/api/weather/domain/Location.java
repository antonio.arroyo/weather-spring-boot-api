package com.api.weather.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {
    @JsonProperty("Key")
    private String key;

    @JsonProperty("Country")
    private Region country;

    @JsonProperty("AdministrativeArea")
    private Region administrativeArea;

    @JsonProperty("EnglishName")
    private String englishName;

}
