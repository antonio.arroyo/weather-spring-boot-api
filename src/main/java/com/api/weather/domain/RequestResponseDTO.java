package com.api.weather.domain;

import com.api.weather.models.Request;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestResponseDTO {

    private List<Request> requestList;

}
