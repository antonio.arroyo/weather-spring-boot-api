package com.api.weather.services;

import com.api.weather.configurations.AccuweatherApiProperties;
import com.api.weather.domain.Conditions;
import com.api.weather.domain.ConditionsCityResponseDTO;
import com.api.weather.domain.ConditionsResponseDTO;
import com.api.weather.domain.Location;
import com.api.weather.exceptions.ApiErrorException;
import com.api.weather.models.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class AccuweatherService {

    @Autowired
    private AccuweatherApiProperties accuweatherApiProperties;
    @Autowired
    private RequestService requestService;

    private final String endpoint = "http://dataservice.accuweather.com";

    private void saveRequest(String url, Object response){
        Request request = new Request();
        request.setUrl(url);
        request.setResponse(response);
        requestService.save(request);
    }

    private Conditions fetchConditions(String locationCode) throws Exception {
        String apiKey = accuweatherApiProperties.getKey();

        String url = String.format("%s/currentconditions/v1/%s?apikey=%s",endpoint, locationCode, apiKey);

        RestTemplate restTemplate = new RestTemplate();
        try{
            Conditions[] conditionsList = restTemplate.getForObject(url, Conditions[].class);
            assert conditionsList != null;

            return conditionsList[0];
        }catch (HttpClientErrorException e){
            throw new ApiErrorException(e.getMessage(), e.getStatusCode());
        }


    }

    private ConditionsResponseDTO buildResponse(Conditions conditions){
        return ConditionsResponseDTO.builder()
                .observationTime(conditions.getLocalObservationDateTime())
                .precipitationType(conditions.getPrecipitationType() != null ? conditions.getPrecipitationType() : "none")
                .temperatureCelsius(conditions.getTemperature().getMetric().getValue())
                .temperatureFahrenheit(conditions.getTemperature().getImperial().getValue())
                .weatherDescription(conditions.getWeatherText())
                .build();
    }

    private ConditionsCityResponseDTO composeResponse(ConditionsResponseDTO conditionsResponseDTO, Location location){

        return ConditionsCityResponseDTO.builder()
                .conditions(conditionsResponseDTO)
                .countryName(location.getCountry().getEnglishName())
                .cityName(location.getEnglishName())
                .stateName(location.getAdministrativeArea().getEnglishName())
                .build();
    }

    public List<Location> getLocationCodeByQuery(String query) {
        String apiKey = accuweatherApiProperties.getKey();
        String url = String.format("%s/locations/v1/cities/search?apikey=%s&q=%s", endpoint, apiKey, query);

        RestTemplate restTemplate = new RestTemplate();
        Location[] city = restTemplate.getForObject(url, Location[].class);
        assert city != null;
        return Arrays.asList(city);
    }


    public ConditionsCityResponseDTO getConditionWithQuery(String query) throws Exception {
        List<Location> location = getLocationCodeByQuery(query);
        if(location.size() == 0) throw new ApiErrorException("City not found", HttpStatus.NOT_FOUND);
        return getConditions(location.get(0), query);
    }

    public ConditionsResponseDTO getConditions(String locationCode) throws Exception {
        Conditions  conditions = fetchConditions(locationCode);

        ConditionsResponseDTO response = buildResponse(conditions);
        String requestUrl = String.format("/conditions/code/%s", locationCode);

        saveRequest(requestUrl, response);

        return response;
    }

    public ConditionsCityResponseDTO getConditions(Location location, String query) throws Exception {
        Conditions  conditions = fetchConditions(location.getKey());
        ConditionsResponseDTO conditionsResponseDTO = buildResponse(conditions);

        ConditionsCityResponseDTO response = composeResponse(conditionsResponseDTO, location);

        String requestUrl = String.format("/conditions/city/%s", query);

        saveRequest(requestUrl, response);

        return response;
    }

}
