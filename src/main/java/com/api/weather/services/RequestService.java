package com.api.weather.services;

import com.api.weather.models.Request;
import com.api.weather.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {

    @Autowired
    RequestRepository requestRepository;

    public List<Request> list(){
        return requestRepository.findAll();
    }

    public Request save(Request payload){
        return requestRepository.save(payload);
    }
}
