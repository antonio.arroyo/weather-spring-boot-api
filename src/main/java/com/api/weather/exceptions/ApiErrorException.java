package com.api.weather.exceptions;

import org.springframework.http.HttpStatus;

public class ApiErrorException extends RuntimeException{

    private HttpStatus httpStatus;
    public ApiErrorException(String errorMessage, HttpStatus status){
        super(errorMessage);
        httpStatus = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }


}
