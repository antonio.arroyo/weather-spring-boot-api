package com.api.weather.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class ApiError {

    private Date timestamp;
    private String error;
    private int status;
    private String path;

}
